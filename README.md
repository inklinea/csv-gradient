# csv Gradient - an Inkscape 1.2+ extension
# Just a basic example of a Linear Gradient from csv (comma delimited)
# No error checking
# Colours in first column of csv
# Stops are evenly spaced
# Accepts css colour names, hex colours \#ff00aa or rbg(100,200, 30)
