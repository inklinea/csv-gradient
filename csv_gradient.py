#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# csv Gradient - an Inkscape 1.2+ extension
# Just a basic example of a Linear Gradient from csv
##############################################################################


import inkex

from inkex import LinearGradient, Stop

import csv

def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None


def make_gradient_object(self, gradient_list):

    gradient_object = LinearGradient()

    # Lets look at the gradient stop list
    number_of_stops = len(gradient_list)

    stop_offset = 0
    stop_offset_fraction = 100 / number_of_stops

    for gradient_entry in gradient_list:

        stop_object = Stop()

        stop_object.set('stop-color', gradient_entry[0])
        stop_object.set('offset', f'{stop_offset}%')

        stop_offset += stop_offset_fraction

        gradient_object.append(stop_object)

    self.svg.xpath('//svg:defs')[0].append(gradient_object)



class CsvGradient(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--csv_filepath", type=str, dest="csv_filepath")

    def effect(self):

        csv_filepath = self.options.csv_filepath

        gradient_list = []

        with open(csv_filepath, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                gradient_list.append(row)

        make_gradient_object(self, gradient_list)

if __name__ == '__main__':
    CsvGradient().run()

